<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Load
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Load newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Load newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Load query()
 * @mixin \Eloquent
 */
class Load extends Model
{
    protected $fillable = [
        'admin_id', 'user_id', 'uploaded_by', 'vehicle_id', 'qty', 'source_city', 'destination_city', 'distance', 'martial_id',
        'weight', 'scheduled_date', 'scheduled_time', 'price_on', 'price', 'instructions'
    ];

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function vehicle()
    {
        return $this->belongsTo(Vehicle::class);
    }

    public function martial()
    {
        return $this->belongsTo(MaterialTypes::class);
    }
}
