<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\User
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property int $mobile
 * @property string $email
 * @property string $username
 * @property string $password
 * @property int $type individual = 1, company = 2
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Sofa\Eloquence\Builder|\App\Models\User newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\User newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereMobile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUsername($value)
 * @mixin \Eloquent
 * @property string|null $profile_image
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereProfileImage($value)
 */
class User extends Model
{
    use Eloquence;

    CONST INDIVIDUAL = 1, COMPANY = 2;

    protected $fillable = [
        'first_name', 'last_name', 'mobile', 'email', 'username', 'password', 'profile_image','type',
    ];

    protected $hidden = [
        'token' , 'password'
    ];

}
