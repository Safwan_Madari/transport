<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\UserCompany
 *
 * @property int $id
 * @property int $load_provider_id
 * @property string|null $name
 * @property string|null $company_type
 * @property string|null $service_tax_no
 * @property string|null $gst_no
 * @property string|null $pan_no
 * @property string|null $website
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserCompany newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserCompany newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserCompany query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserCompany whereCompanyType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserCompany whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserCompany whereGstNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserCompany whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserCompany whereLoadProviderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserCompany whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserCompany wherePanNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserCompany whereServiceTaxNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserCompany whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserCompany whereWebsite($value)
 * @mixin \Eloquent
 */
class UserCompany extends Model
{
    use Eloquence;
    protected  $fillable = [
        'user_id', 'name', 'company_type', 'service_tax_no', 'gst_no', 'pan_no', 'website'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
