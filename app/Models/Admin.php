<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\admin
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $mobile
 * @property string $username
 * @property string $password
 * @property int $status 1 = Active, 2 = In-Active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\admin newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\admin newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\admin query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\admin whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\admin whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\admin whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\admin whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\admin whereMobile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\admin wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\admin whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\admin whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\admin whereUsername($value)
 * @mixin \Eloquent
 * @property-read mixed $full_name
 */
class admin extends Model
{
    use Eloquence;

    protected $fillable = ['first_name', 'last_name', 'mobile', 'username', 'password', 'status'];

    public function getFullNameAttribute()
    {
        return $this->first_name .' '. $this->last_name;
    }
}
