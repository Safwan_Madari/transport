<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\UserAddress
 *
 * @property-read \App\Models\User $loadProvider
 * @method static \Sofa\Eloquence\Builder|\App\Models\UserAddress newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\UserAddress newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\UserAddress query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $load_provider_id
 * @property string|null $address
 * @property string|null $city
 * @property int|null $state_id
 * @property int|null $pincode
 * @property string|null $pan_no
 * @property string|null $alternate_contact_person
 * @property int|null $alternate_mobile
 * @property int|null $std_code
 * @property int|null $landline_no
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAddress whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAddress whereAlternateContactPerson($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAddress whereAlternateMobile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAddress whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAddress whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAddress whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAddress whereLandlineNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAddress whereLoadProviderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAddress wherePanNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAddress wherePincode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAddress whereStateId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAddress whereStdCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAddress whereUpdatedAt($value)
 */
class UserAddress extends Model
{
    use Eloquence;

    protected $fillable = [
        'user_id', 'address', 'city', 'state_id', 'pincode', 'pan_no', 'alternate_contact_person', 'alternate_mobile' , 'std_code', 'landline_no'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
