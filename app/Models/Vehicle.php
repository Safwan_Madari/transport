<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;
use App\Models\Admin;

/**
 * App\Models\Vehicle
 *
 * @property int $id
 * @property int $admin_id
 * @property string $name
 * @property float $capacity
 * @property string $image
 * @property int $status 1: Active, 2:In-Active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\admin $admin
 * @method static \Sofa\Eloquence\Builder|\App\Models\Vehicle newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\Vehicle newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\Vehicle query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle whereCapacity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vehicle whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Vehicle extends Model
{
    use Eloquence;

    const ACTIVE = 1, IN_ACTIVE = 2;


    protected $fillable = [
        'admin_id', 'name', 'capacity', 'image', 'status'
    ];

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    public function ScopeActive()
    {
        return $this->whereStatus($this::ACTIVE);
    }
}
