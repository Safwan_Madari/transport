<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;
use App\Models\Admin;

/**
 * App\Models\MaterialTypes
 *
 * @property-read \App\Models\admin $admin
 * @method static \Sofa\Eloquence\Builder|\App\Models\MaterialTypes newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\MaterialTypes newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\MaterialTypes query()
 * @mixin \Eloquent
 */
class MaterialTypes extends Model
{
    use Eloquence;
    const ACTIVE = 1, IN_ACTIVE = 2;

    protected $fillable = [
        'admin_id', 'name'. 'status'
    ];

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    public function ScopeActive()
    {
        return $this->whereStatus($this::ACTIVE);
    }
}
