<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UserAddress;
use App\Models\UserCompany;
use App\Models\State;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;

class RegisterController extends Controller
{
    public function types()
    {
        return view('user.authentication.register.register-types');
    }

//    registration for only load provider
    public function loadProviderRegister(Request $request)
    {

        if($request->isMethod('post')){
            $this->validate($request,[
                'type' => 'required',
                'first_name' => 'required',
                'last_name' => 'required',
                'mobile' => 'required|regex:/^[9876][0-9]{9}$/|digits:10',
                'email' => 'email',
                'password' => 'required|min:6',
                'confirm_password' => 'required|same:password',
                'city' => 'required',
                'state_id' => 'required',
                'pincode' => 'required|digits:6',
                'pan_no' => 'required|regex:/^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/',
                'company_name' => 'required_if:type,2',
                'company_type' => 'required_if:type,2',
                'company_pan_no' => 'required_if:type,2|regex:/^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/',
            ],[
                'type.required' => 'Please Select Registration type',
                'first_name.required' => 'First name is required',
                'last_name.required' => 'Last name is required',
                'mobile.required' => 'Mobile Number is required',
                'mobile.regex' => 'Invalid Mobile Number',
                'email.email' => 'Invalid Email Address',
                'password.required' => 'Password is required',
                'password.min' => 'Password is required minimum six character',
                'confirm_password.required' => 'Confirm Password is required',
                'confirm_password.same' => 'Confirm Password is not match',
                'city.required' => 'City is required',
                'state_id.required' => 'Please Select State',
                'pincode.required' => 'Pincode is required',
                'pincode.digits' => 'Pincode Must be 6 digit number',
                'pan_no.required' => 'PanCard Number is required',
                'pan_no.regex' => 'PanCard Number not valid',
                'company_name.required_if' => 'Company Name is required.',
                'company_type.required_if' => 'Company Type is required',
                'company_pan_no.required_if' => 'Company PanCard Number is required',
                'company_pan_no.regex' => 'Company Pan Card Number not valid',

            ]);


            \DB::transaction(function () use ($request) {


                if($request->hasFile('profile_image') && $request->file('profile_image')->isValid())
                {
                    $store = \Storage::disk('public_uploads');
                    $image = Uuid::generate(4).$request->file('profile_image')->getClientOriginalName();
                    $store->put(env('PROFILE_IMAGE_PATH').$image, file_get_contents($request->file('profile_image')->path()));

                }
                $loadProvider = User::create([
                    'first_name' => $request->first_name,
                    'last_name' => $request->last_name,
                    'mobile' => $request->mobile,
                    'email' => $request->email,
                    'username' => $request->mobile,
                    'password' => $request->password,
                    'profile_image' => $image,
                    'type' => $request->type
                ]);

                UserAddress::create([
                    'load_provider_id' => $loadProvider->id,
                    'address' => $request->address,
                    'city' => $request->city,
                    'state_id' => $request->state_id,
                    'pincode' => $request->pincode,
                    'pan_no' => $request->pan_no,
                    'alternate_contact_person' => $request->alternate_contact_person,
                    'alternate_mobile' => $request->alternate_mobile,
                    'std_code' => $request->std_code,
                    'landline_no' => $request->landline_no
                ]);

                UserCompany::create([
                    'load_provider_id' => $loadProvider->id,
                    'name' => $request->company_name,
                    'company_type' => $request->company_type,
                    'service_tax_no' => $request->service_tax_no,
                    'gst_no' => $request->gst_no,
                    'pan_no' => $request->company_pan_no,
                    'website' => $request->website
                ]);

            });
            return redirect()->route('load-provider-register')->with(['success' => 'Congratulation, You have Successfully Registered ']);

        }
        return view('user.authentication.register.load-provider',[
            'states' => State::active()->get()
        ]);
    }

    public function index(Request $request)
    {
        return view('user.authentication.register.index');
    }

}
