<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\MaterialTypes;
use App\Models\Vehicle;
use Illuminate\Http\Request;

class LoadController extends Controller
{

    public function index()
    {
        return view('user.load.index');
    }

    public function create(Request $request)
    {
        if($request->isMethod('post')){
            $this->validate($request,[
                'source_city' => 'required',
                'destination_city' => 'required',
                'qty' => 'required|min:1',
                'material_id' => 'required',
                'vehicle_id' => 'required',
                'weight' => 'required|min:1',
                'scheduled_date' => 'required',
                'price_on' => 'required',
                'price' => 'required|numeric|min:1',
            ],[
                'source_city.required' => 'Please Select Source City',
                'destination_city.required' => 'Please Select Destination City',
                'qty.required' => 'Enter Number of Required Vehicle',
                'qty.min' => 'Entered Number of Required Vehicle is invalid',
                'material_id.required' => 'Please Select Material Type ',
                'vehicle_id.required' => 'Please Select Vehicle Type ',
                'weight.required' => 'Please Enter Weight of Goods',
                'weight.min' => 'You Entered Weight is Not Valid',
                'scheduled_date.required' => 'Please Select Scheduled Date',
                'price_on.required' => 'Please Select Scheduled Date',
                'price.required' => 'Please Enter Pent Date',
            ]);
        }

        return view('user.load.create',[
            'vehicles' => Vehicle::active()->get(),
            'materials' => MaterialTypes::active()->get(),
        ]);
    }

    public function direction(Request $request)
    {
        $user = \Session::get('user');

        dd($request->all(),$user);
    }
}
