<?php

namespace App\Http\Controllers\User;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        if(\Session::get('user'))
            return redirect()->route('user-dashboard');

        if ($request->isMethod('post')){
            $this->validate($request,[
                'mobile' => 'required|regex:/^[9876][0-9]{9}$/|digits:10',
                'password' => 'required'
            ],[
                'mobile.required' => 'Mobile Number is Required',
                'mobile.regex' => 'Invalid Mobile Number',
                'mobile.digits' => 'Mobile Number is must be Numeric Value'
            ]);

            if(! $loadProvider = User::whereMobile($request->mobile)->first()) {
                return redirect()->back()->with(['error' => 'This Mobile Number is Not Registered, please Register your self before Login ']);
            }
            if ($loadProvider->password == $request->password){

                \Session::put('user', $loadProvider);

                return redirect()->route('user-dashboard')->with(['success' => 'Welcome '. $loadProvider->first_name . ' to ' . env('COMPANY_NAME') . ' Account' ]);
            }
            else{
                return redirect()->back()->with(['error' => 'Invalid Password for this mobile number']);

            }
        }
        return view('user.authentication.login');
    }

    public function logout()
    {
        \Session::forget('user');

        return redirect()->route('user-login')->with(['success' => 'Good buy.. Logout Successfully']);
    }
}

