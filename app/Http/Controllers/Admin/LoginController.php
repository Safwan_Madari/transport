<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin;


class LoginController extends Controller
{
    public function login(Request $request)
    {

        if($request->isMethod('post')){

            $this->validate($request,[
                'username' => 'required',
                'password' => 'required'
            ],[
                'username.required' => 'Username is Required',
                'password.required' => 'Password is Required'
            ]);

            if(!$admin = Admin::whereUsername($request->username)->first())
                return redirect()->back()->with(['error' => 'You Entered Wrong Username.']);

            if(\Hash::check($request->password, $admin->password)) {

                \Session::put('admin', $admin);

                return redirect()->route('admin-dashboard')->with(['success' => 'Welcome ' . $admin->first_name ]);
            }
            else {
                return redirect()->route('admin-login')->with(['error' => 'Wrong Username Or Password']);
            }
        }
        return view('admin.authentication.login');
    }

    public function logout()
    {
       \Session::forget('admin');

       return redirect()->route('admin-login')->with(['success' => 'Logout Successfully.. Good Bay']);
    }
}
