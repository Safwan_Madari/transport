<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminManagerController extends Controller
{
    public function index(Request $request)
    {
        $admins = Admin::search($request->search, [
            'first_name','last_name','mobile','username'
        ]);

        return view('admin.admin-manager.index',[
            'admins' => $admins->orderBy('id')->paginate(50)
        ]);
    }

    public function create(Request $request)
    {
        if($request->isMethod('post')){

            $this->validate($request,[
                'first_name' => 'required',
                'last_name' => 'required',
                'mobile' => 'required|regex:/^[9876][0-9]{9}$/|digits:10|unique:admins,mobile',
                'username' => 'required|unique:admins,username',
                'password' => 'required|min:6',
                'confirm_password' => 'required|same:password'
            ],[
                'first_name.required' => 'First Name is required',
                'last_name.required' => 'Last Name is required',
                'mobile.required' => 'Mobile Number is Required',
                'mobile.unique' => 'This Mobile Number is already exist',
                'mobile.regex' => 'Invalid Mobile Number',
                'mobile.digits' => 'Mobile Number must be 10 digits',
                'username.required' => 'Username is required',
                'username.unique' => 'Username already Exists, try another',
                'password.required' => 'Password is required',
                'password.min' => 'Password is required minimum 6 Characters',
                'confirm_password.same' => 'Password is Not Match With Confirm Password',
            ]);

            Admin::create([
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'mobile' => $request->mobile,
                'username' => $request->username,
                'password' => \Hash::make($request->password),
            ]);

            return redirect()->route('admin-manager-create')->with(['success' => 'New Admin Created']);
        }

        return view('admin.admin-manager.create');
    }

    public function update(Request $request)
    {
        if (!$admin = Admin::whereId($request->id)->first())
            return redirect()->back()->with(['error' => 'Invalid Admin Request']);

        if($request->isMethod('post')){
            $this->validate($request,[
                'first_name' => 'required',
                'last_name' => 'required',
                'mobile' => 'required|regex:/^[9876][0-9]{9}$/|digits:10',
                'username' => 'required',
            ],[
                'first_name.required' => 'First Name is required',
                'last_name.required' => 'Last Name is required',
                'mobile.required' => 'Mobile Number is Required',
                'mobile.regex' => 'Invalid Mobile Number',
                'mobile.digits' => 'Mobile Number must be 10 digits',
                'username.required' => 'Username is required',
            ]);

            $admin->first_name = $request->first_name;
            $admin->last_name = $request->last_name;
            $admin->mobile = $request->mobile;
            $admin->username = $request->username;
            $admin->status = $request->status;
            $admin->save();

            return redirect()->route('admin-manager-view')->with(['success' => 'Admin Details has been Updated']);

        }

        return view('admin.admin-manager.update',[
            'admin' => $admin
        ]);
    }
}
