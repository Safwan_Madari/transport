<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index()
    {
       $admin = \Session::get('admin');

        return view('admin.dashboard',[
            'admin' => $admin
        ]);
    }
}
