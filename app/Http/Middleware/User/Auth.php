<?php

namespace App\Http\Middleware\User;

use Closure;

class Auth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(! \Session::get('user'))
            return redirect()->route('user-login')->with(['error' => 'Session Expire Please Login Again']);
        return $next($request);
    }
}
