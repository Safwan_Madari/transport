<?php

Route::group(['namespace' => 'User','prefix' => 'user'], function(){

    Route::group(['prefix' => 'register'], function (){

        Route::get('/','RegisterController@types')->name('user-register-types');

        Route::get('load-provider','RegisterController@loadProviderRegister')->name('load-provider-register');
        Route::post('load-provider','RegisterController@loadProviderRegister')->name('load-provider-register');

    });


    Route::get('login','LoginController@login')->name('user-login');
    Route::post('login','LoginController@login')->name('user-login');

    Route::get('logout','LoginController@logout')->name('user-logout');

    Route::group(['middleware' => 'userAuth'], function () {

        Route::get('dashboard','DashboardController@index')->name('user-dashboard');

        Route::group([ 'prefix' => 'post-load'], function(){

            Route::get('/','LoadController@index')->name('user-post-load-view');

            Route::get('create','LoadController@create')->name('user-post-load-create');
            Route::post('create','LoadController@create')->name('user-post-load-create');

            Route::get('direction','LoadController@direction')->name('user-post-load-find-direction');


        });
    });

});


