<?php

Route::group(['namespace' => 'Admin','prefix' => 'admin-panel'], function(){


    Route::get('/','LoginController@login')->name('admin-login');
    Route::post('/','LoginController@login')->name('admin-login');

    Route::group(['middleware' => 'admin'],function(){

        Route::get('logout','LoginController@logout')->name('admin-logout');

        Route::get('dashboard','DashboardController@index')->name('admin-dashboard');



        Route::group(['prefix' => 'admin-manager' ] ,function (){

            Route::get('/','AdminManagerController@index')->name('admin-manager-view');

            Route::get('create','AdminManagerController@create')->name('admin-manager-create');
            Route::post('create','AdminManagerController@create')->name('admin-manager-create');

            Route::get('update/{id}','AdminManagerController@update')->name('admin-manager-update');
            Route::post('update/{id}','AdminManagerController@update')->name('admin-manager-update');
        });

    });

});