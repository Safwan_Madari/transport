<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->string('address')->nullable();
            $table->string('city')->nullable();
            $table->integer('state_id')->unsigned()->nullable();
            $table->integer('pincode')->nullable();
            $table->string('pan_no')->nullable();
            $table->string('alternate_contact_person')->nullable();
            $table->string('alternate_mobile',20)->nullable();
            $table->integer('std_code')->nullable();
            $table->integer('landline_no')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_addresses');
    }
}
