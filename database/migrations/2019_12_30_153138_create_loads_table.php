<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loads', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('admin_id')->unsigned()->index()->nullable();
            $table->integer('user_id')->unsigned()->index()->nullable();
            $table->tinyInteger('uploaded_by')->comment('1: Load Provider, 2: Transport Company, 3:Agent');
            $table->integer('vehicle_id')->unsigned()->index();
            $table->integer('qty')->default(1);
            $table->string('source_city');
            $table->string('destination_city');
            $table->string('distance')->nullable();
            $table->integer('martial_id')->unsigned();
            $table->float('weight',10,2)->nullable();
            $table->dateTime('scheduled_date');
            $table->dateTime('scheduled_time')->nullable();
            $table->tinyInteger('price_on')->default(1)->comment('Per Ton = 1, Full Truck = 2');
            $table->float('price',10,2);
            $table->longText('instructions');
            $table->tinyInteger('status')->default(1)->comment('Open = 1, Running = 2, Canceled = 3, Completed = 4');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loads');
    }
}
