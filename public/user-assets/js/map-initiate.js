
function initMap() {
    var latInput = $('#latInput');
    var lngInput = $('#lngInput');
    var searchInput = $('#mapSearch');
    var infoWindow = new google.maps.InfoWindow({
        map: map
    });

    if(latInput.val() && lngInput.val())
    {
        var mapCenter = {
            lat: parseFloat(latInput.val()),
            lng: parseFloat(lngInput.val())
        }
    }
    else
    {
        var mapCenter = {
            lat: 22.7196,
            lng: 75.8577
        };
        geoLocate();
    }


    var map = new google.maps.Map(document.getElementById('map'), {
        center: mapCenter,
        zoom: 12,
        mapTypeId: 'hybrid'
    });

    // Create the search box and link it to the UI element.
    searchInput.show();
    var input = searchInput[0];
    var searchBox = new google.maps.places.SearchBox(input);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    // Bias the SearchBox results towards current map's viewport.
    map.addListener('bounds_changed', function() {
        searchBox.setBounds(map.getBounds());
    });

    var marker = new google.maps.Marker({
        position: mapCenter,
        map: map,
        draggable: true,
    });

    marker.addListener('dragend', function(e) {
        latInput.val((e.latLng.lat()).toFixed(4));
        lngInput.val((e.latLng.lng()).toFixed(4));
    });

    map.addListener('click', function(e) {
        marker.setPosition(e.latLng);

        latInput.val((e.latLng.lat()).toFixed(4));
        lngInput.val((e.latLng.lng()).toFixed(4));

    });

    searchBox.addListener('places_changed', function() {
        var places = searchBox.getPlaces();

        if (places.length == 0) {
            return;
        }

        map.setCenter(places[0].geometry.location);
        marker.setPosition(places[0].geometry.location);
        latInput.val(places[0].geometry.location.lat());
        lngInput.val(places[0].geometry.location.lng());
    });

    function geoLocate() {
        // Try HTML5 Geolocation.
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };

                map.setCenter(pos);
                marker.setPosition(pos);
            }, function() {
                handleLocationError(true, infoWindow, map.getCenter());
            });
        } else {
            // Browser doesn't support Geolocation
            handleLocationError(false, infoWindow, map.getCenter());
        }
    }

    function handleLocationError(browserHasGeolocation, infoWindow, pos) {
        infoWindow.setPosition(pos);
        infoWindow.setContent(browserHasGeolocation ? 'Error: The Geolocation service failed.' : 'Error: Your browser doesn\'t support geolocation.');
    }
}
