@extends('website.template.layout')

@section('page-title')
    Home | |  IT Company in Vadodara | Top IT Company in Baroda | Software Development Company in Vadodara(Baroda) | Web Development Company in Vadodara | Web Development Services Vadodara - Wordpress Development company Vadodara (Gujarat) | Web Designing Services Vadodara | Web Designing Development Company India | PHP Web Development Services | php web development Services in Vadodara | Software Development services Vadodara | Custom Software Development Company - India | Webserve Technology-IT Companies in Vadodara | Top Web Design Company in Vadodara | Top Web Development company in vadodara | Top Software Development Company in Vadodara(Baroda) | Web Designing Company in Vadodara(Baroda) | Website Development Company in Vadodara(Baroda) | PHP Development Company in Vadodara(Baroda) | SEO Company in Vadodara(Baroda) | Digital Marketing Company in Vadodara(Baroda)
    @endsection()

@section('page-content')

    <div class="rev-slider-container">
        <div class="tp-banner-container">
            <div class="tp-banner" >
                <ul>
                    <!-- SLIDE  -->
                    <li data-delay="4000" data-masterspeed="600" data-slotamount="7" data-transition="scaledownfromtop">
                        <!-- MAIN IMAGE -->
                        <img src="/website-assets/images/slider/slider01.jpg" alt="">

                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption with_white_background skewfromleft tp-resizeme"
                             data-x="65"
                             data-y="61"
                             data-speed="1000"
                             data-start="300"
                             data-easing="Power3.easeInOut"
                             data-endspeed="300"
                             style="z-index: 5">WEB DEVELOPMENT</div>

                        <!-- LAYER NR. 2 -->
                        <div class="tp-caption with_colored_background skewfromleft tp-resizeme"
                             data-x="63"
                             data-y="118"
                             data-speed="1000"
                             data-start="600"
                             data-easing="Power3.easeInOut"
                             data-endspeed="300"
                             style="z-index: 6">WEB DESIGNING</div>

                        <!-- LAYER NR. 3 -->
                        <div class="tp-caption with_white_background skewfromleft tp-resizeme"
                             data-x="63"
                             data-y="176"
                             data-speed="1000"
                             data-start="900"
                             data-easing="Power3.easeInOut"
                             data-endspeed="300"
                             style="z-index: 7">MOBILE APP DEVELOPMENT</div>

                        <!-- LAYER NR. 4 -->
                        <div class="tp-caption with_colored_background skewfromleft tp-resizeme"
                             data-x="64"
                             data-y="233"
                             data-speed="1000"
                             data-start="1200"
                             data-easing="Power3.easeInOut"
                             data-endspeed="300"
                             style="z-index: 8">GRAPHICS DESIGNING </div>

                        <!-- LAYER NR. 5 -->
                        <div class="tp-caption with_white_background skewfromleft tp-resizeme"
                             data-x="63"
                             data-y="290"
                             data-speed="1000"
                             data-start="1500"
                             data-easing="Power3.easeInOut"
                             data-endspeed="300"
                             style="z-index: 9">DIGITAL MARKETING</div>


                        <!-- LAYER NR. 5 -->
                        <div class="tp-caption with_colored_background skewfromleft tp-resizeme"
                             data-x="65"
                             data-y="348"
                             data-speed="1000"
                             data-start="1800"
                             data-easing="Power3.easeInOut"
                             data-endspeed="300"
                             style="z-index: 10">and much more... </div>

                        <div class="tp-caption sfb"
                             data-x="559"
                             data-y="14"
                             data-speed="500"
                             data-start="900"
                             data-easing="Power3.easeInOut"
                             data-endspeed="300"
                             style="z-index: 3"><img src="/website-assets/images/ipad1.png" alt=""> </div>

                        <!-- LAYER NR. 4 -->
                        <div class="tp-caption sfb"
                             data-x="409"
                             data-y="230"
                             data-speed="300"
                             data-start="1400"
                             data-easing="Power3.easeInOut"
                             data-endspeed="300"
                             style="z-index: 4"><img src="/website-assets/images/iphone1.png" alt=""> </div>

                    </li>
                    <!-- SLIDE  -->
                    <li data-delay="4000" data-masterspeed="600" data-slotamount="7" data-transition="fade">
                        <!-- MAIN IMAGE -->
                        <img src="/website-assets/images/slider/slider2.jpg" alt="">
                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption tentered_white_huge lft tp-resizeme" data-endspeed="300" data-easing="Power4.easeOut" data-start="400" data-speed="600" data-y="180" data-hoffset="0" data-x="center"><strong>Simple Solutions </strong></div>
                        <!-- LAYER NR. 2 -->
                        <div class="tp-caption tentered_white_huge lfb tp-resizeme" data-endspeed="300" data-easing="Power4.easeOut" data-start="400" data-speed="600" data-y="260" data-hoffset="0" data-x="center"> for Complex Connections</div>
                    </li>
                    <li data-masterspeed="600" data-slotamount="7" data-transition="parallaxtotop">
                        <!-- MAIN IMAGE -->
                        <img src="/website-assets/images/slider/slider3.jpg" alt="">
                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption with_colored_background sft tp-resizeme start" data-endspeed="300" data-easing="Power4.easeInOut" data-start="2000" data-speed="600" data-y="186" data-x="30">24*7 Best Service for You</div>
                        <!-- LAYER NR. 2 -->
                        <div class="tp-caption with_white_background sft tp-resizeme start" data-endspeed="300" data-easing="Power4.easeInOut" data-start="1800" data-speed="600" data-y="254" data-x="30">Offering a wide range of diverse
                            <br> services to partner and clients.</div>

                        {{--<!-- LAYER NR. 3 -->--}}
                        <div class="tp-caption sfb"
                             data-x="559"
                             data-y="14"
                             data-speed="500"
                             data-start="900"
                             data-easing="Power3.easeInOut"
                             data-endspeed="300"
                             style="z-index: 3"><img src="/website-assets/images/home/managed-web-hosting.png" alt=""> </div>

                        <!-- LAYER NR. 4 -->
                        {{--<div class="tp-caption sfb"--}}
                        {{--data-x="409"--}}
                        {{--data-y="230"--}}
                        {{--data-speed="300"--}}
                        {{--data-start="1400"--}}
                        {{--data-easing="Power3.easeInOut"--}}
                        {{--data-endspeed="300"--}}
                        {{--style="z-index: 4"><img src="/website-assets/images/iphone1.png" alt=""> </div>--}}
                    </li>
                </ul>
            </div>
        </div>
    </div>
    {{--slider ends--}}
    <div class="featured-row orange-color margin-20">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="text-white">WE ARE COMMITTED TO DELIVERING EXCELLENCE IN IT SERVICES</h2>
                    <a href="#" class="btn btn-primary">Know more</a> <a href="javascript:void(0)" class="btn transparent">Get In Touch</a> </div>
            </div>
        </div>
    </div>

    <div class="padding-tb75">
        <div class="container text-align-center">
            <div class="row">
                <div class="col-md-12 margin-20 text-align-center">
                    <h2>Why work with us?</h2>
                    <p>Choosing a technology solutions partner is a long-term endeavour. Whether building an online presence or a customized software solution, it pays to check the facts before empanelling a technology partner for your business. We are happy to help you!</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-sm-6 appear-animation fadeIn appear-animation-visible features-main" data-appear-animation="fadeIn" data-appear-animation-delay="50" style="animation-delay: 50ms;">
                    <section class="features"> <span class="features-icon"><i class="fa fa-building-o"></i></span>
                        <h4 class="text-white">Industry Experience</h4>
                        <p>3+ years in business
                            Domain expertise across industry verticals
                            Complete ‘Digital’ Solutions under one roof</p>
                    </section>
                </div>
                <div class="col-md-3 col-sm-6 appear-animation fadeIn appear-animation-visible features-main" data-appear-animation="fadeIn" data-appear-animation-delay="100" style="animation-delay: 100ms;">
                    <section class="features"> <span class="features-icon"><i class="fa fa-headphones"></i></span>
                        <h4 class="text-white">Approach</h4>
                        <p>Follow Industry Standard Best Practices
                            Agile Development Process.</p>
                    </section>
                </div>
                <div class="col-md-3 col-sm-6 appear-animation fadeIn appear-animation-visible features-main" data-appear-animation="fadeIn" data-appear-animation-delay="150" style="animation-delay: 150ms;">
                    <section class="features"> <span class="features-icon"><i class="fa fa-mobile"></i></span>
                        <h4 class="text-white">Communication</h4>
                        <p>Single point of contact for all your projects
                            Available via Phone, Email and Chat
                            Clear English Communication</p>
                    </section>
                </div>
                <div class="col-md-3 col-sm-6 appear-animation fadeIn appear-animation-visible features-main" data-appear-animation="fadeIn" data-appear-animation-delay="200" style="animation-delay: 200ms;">
                    <section class="features"> <span class="features-icon"><i class="fa fa-thumbs-o-up"></i></span>
                        <h4 class="text-white">Ongoing Support</h4>
                        <p>Ongoing post production support
                            Request a free proposal without any obligation
                            Complete confidentiality for your ideas</p>
                    </section>
                </div>
            </div>
        </div>
    </div>


    <section class="parallax parallax1" style="background-image: url(&quot;images/work-bg.jpg&quot;); background-position: 50% 31px;">
        <div class="black-overlay padding-tb75">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 appear-animation bounceInLeft appear-animation-visible" data-appear-animation="bounceInLeft">
                        <h2 class="color-text">Our Qualities</h2>
                        <div class="tabs">
                            <ul class="nav nav-tabs">
                                <li class=""> <a data-toggle="tab" href="#sampletab1"> HOW WE DO IT? </a> </li>
                                <li class="active"> <a data-toggle="tab" href="#sampletab2"> OUR MISSION </a> </li>
                                <li class=""> <a data-toggle="tab" href="#sampletab3"> OUR VISION </a> </li>
                            </ul>
                            <div class="tab-content">
                                <div id="sampletab1" class="tab-pane">
                                    <div class="media">

                                        <p><i class="fa fa-hand-o-right"></i> We have a keen understanding of people, processes and technology. We keep abreast of emerging trends and evolving business practices.</p>
                                        <p><i class="fa fa-hand-o-right"></i> Our team are trained to understand, anticipate and respond to your needs / queries intelligently.</p>
                                        <p><i class="fa fa-hand-o-right"></i> We believe in delivering a business solution and not an IT solution.</p>
                                    </div>
                                </div>
                                <div id="sampletab2" class="tab-pane active">
                                    <div class="media">
                                        <p><i class="fa fa-hand-o-right"></i> At Maa Softech, we have a mission to ensure that we help our clients achieve their business objective efficiently. We make sure that our client's get cost effective solutions for their business needs.</p>
                                    </div>
                                </div>
                                <div id="sampletab3" class="tab-pane">
                                    <p><i class="fa fa-hand-o-right"></i> For our client's satisfaction, we always emphasis on availability, affordability and accessibility of our services accompanied by our passion for innovation and prolonged services.</p>
                                    <p><i class="fa fa-hand-o-right"></i> We also have a vision of imparting technical skills to young and inexperienced IT graduates on a voluntary basis so that they can easily launch their career in IT.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 appear-animation bounceInRight appear-animation-visible" data-appear-animation="bounceInRight" data-appear-animation-delay="200" style="animation-delay: 200ms;">
                        <h2 class="color-text">We Are Work On</h2>
                        <div class="progress progress-striped">
                            <div class="progress-bar" data-appear-progress-animation="95%" style="width: 95%;"> Web Development </div>
                        </div>
                        <div class="progress progress-striped">
                            <div class="progress-bar" data-appear-progress-animation="80%" data-appear-animation-delay="200" style="animation-delay: 200ms; width: 80%;"> Web Design </div>
                        </div>
                        <div class="progress progress-striped">
                            <div class="progress-bar" data-appear-progress-animation="75%" data-appear-animation-delay="300" style="animation-delay: 300ms; width: 75%;"> Mobile Application </div>
                        </div>
                        <div class="progress progress-striped">
                            <div class="progress-bar" data-appear-progress-animation="89%" data-appear-animation-delay="400" style="animation-delay: 400ms; width: 89%;"> Graphics Design </div>
                        </div>
                        <div class="progress progress-striped">
                            <div class="progress-bar" data-appear-progress-animation="76%" data-appear-animation-delay="500" style="animation-delay: 500ms; width: 76%;"> Digital Marketing </div>
                        </div>
                        <div class="progress progress-striped">
                            <div class="progress-bar" data-appear-progress-animation="76%" data-appear-animation-delay="600" style="animation-delay: 600ms; width: 76%;"> SEO </div>
                        </div>
                        <div class="progress progress-striped">
                            <div class="progress-bar" data-appear-progress-animation="60%" data-appear-animation-delay="700" style="animation-delay: 700ms; width: 60%;"> EMBEDDED SOFTWARE</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="counters padding-tb45 accent-color text-align-center hidden-sm hidden-xs appear-animation bounceInUp appear-animation-visible" data-appear-animation="bounceInUp" data-appear-animation-delay="200" style="animation-delay: 200ms;">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="fact-ico"> <i class="fa fa-users fa-4x"></i> </div>
                    <div class="clearfix"></div>
                    <div class="timer" data-perc="72"> <span class="count">72</span>+ </div>
                    <div class="clearfix"></div>
                    <span class="fact">CLIENTS</span> </div>
                <div class="col-md-3">
                    <div class="fact-ico"> <i class="fa fa-check-square-o fa-4x"></i> </div>
                    <div class="clearfix"></div>
                    <div class="timer" data-perc="80"> <span class="count">80</span>+ </div>
                    <div class="clearfix"></div>
                    <span class="fact">PROJECT COMPLETED</span> </div>
                <div class="col-md-3">
                    <div class="fact-ico"> <i class="fa fa-code fa-4x"></i> </div>
                    <div class="clearfix"></div>
                    <div class="timer" data-perc="165000"> <span class="count">165000</span>+ </div>
                    <div class="clearfix"></div>
                    <span class="fact">LINES OF CODE</span> </div>
                <div class="col-md-3">
                    <div class="fact-ico"> <i class="fa fa-coffee fa-4x"></i> </div>
                    <div class="clearfix"></div>
                    <div class="timer" data-perc="652"> <span class="count">652</span>+ </div>
                    <div class="clearfix"></div>
                    <span class="fact">CUP OF COFFEE</span> </div>
            </div>
        </div>
    </section>



@stop