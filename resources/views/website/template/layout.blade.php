<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US">
<head>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="website-assets/images/logo/favicon.png"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>@yield('page-title') | Maa Softech hom</title>

    <meta name="description" content="We offers Website Designing, Web Application Development, E-commerce solutions and Online Marketing. Our professionals have proven industry and technical experience and use pioneering techniques and standard methodologies to provide innovative solutions in time. With an experience of over 3 years Maa Softech, professional Web Designing Company has become one of the foremost choices of people looking for professional web Design & Devlopment Company in Vadodara.">
    <meta name="keywords" content="IT Company in Vadodara | Top IT Company in Baroda | Software Development Company in Vadodara(Baroda) | Web Development Company in Vadodara | Web Development Services Vadodara - Wordpress Development company Vadodara (Gujarat) | Web Designing Services Vadodara | Web Designing Development Company India | PHP Web Development Services | php web development Services in Vadodara | Software Development services Vadodara | Custom Software Development Company - India | Webserve Technology-IT Companies in Vadodara | Top Web Design Company in Vadodara | Top Web Development company in vadodara | Top Software Development Company in Vadodara(Baroda) | Web Designing Company in Vadodara(Baroda) | Website Development Company in Vadodara(Baroda) | PHP Development Company in Vadodara(Baroda) | SEO Company in Vadodara(Baroda) | Digital Marketing Company in Vadodara(Baroda)">
    <meta name="abstract" content="web development companies in vadodara, web designing companies in vadodara, software development companies in india, Open source web development companies,PHP development companies, wordpress website development companies in vadodara, Joomla website development company in vadodara, magneto website development in vadodara, open source development company vadodara, domain booking in vadodara, web hosting companies in baroda,IT Companies in vadodara, Web Development Company in vadodara, Software Development Company in vadodara, Customize Solutions in vadodara, gujarat, India" />

    <meta name="Email" content="maasoftechvadodara@gmail.com" />
    <meta name="copyright" content="Maa Softech." />
    <meta name="robots" content="index, follow"  />
    <meta name="language" content="English" />
    <meta name="reply-to" content="maasoftechvadodara@gmail.com" />

    <meta name="author" content="Maa Softech">

    <meta name="revisit-after" content="10 days"   />

    <meta name="distribution"  content="global"  />

    <meta name="rating" content="general"  />

    {{--<meta name="YahooSeeker" content="INDEX, FOLLOW"  />--}}

    {{--<meta name="msnbot" content="INDEX, FOLLOW"  />--}}

    <meta name="googlebot" content="INDEX, FOLLOW"  />

    <meta name="allow-search" content="yes"  />

    <meta name="page-topic" content="Web Designing Company, Web development company India,software development company"  />

    <meta name="page-type" content="Rich Internet Media"  />

    <meta name="site" content="Web Development Company in vadodara"  />

    <meta name="geo.placename" content="India"  />

    <meta name="og_site_name" property="og:site_name" content="maasoftech.com"/>

    <meta name="geo.region" content="Gujarat"  />

    <meta name="google-site-verification" content="hDxKtQHos0TGXd-8NhakOoQ3o8jyAmHjFlUY3I4Y_lA" />


    <meta property="og:title" content="No 1 Web Development and Design Company in Gujrat " />
    <meta property="og:description" content="We offers Website Designing, Web Application Development, E-commerce solutions and Online Marketing. Our professionals have proven industry and technical experience and use pioneering techniques and standard methodologies to provide innovative solutions in time." />
    <meta property="og:url" content="http://maasoftech.com" />
    <meta property="og:type" content="website" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:image" content="ttp://maasoftech.com/website-assets/images/home/hand-drawn-business.jpg" />



    <!-- Mobile Specific Metas
      ================================================== -->
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="format-detection" content="telephone=9173705320,9879859202,8200286513">
    <!-- CSS
      ================================================== -->
    <link href="website-assets/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="website-assets/css/style.css" rel="stylesheet" type="text/css">
    <link href="website-assets/plugins/prettyphoto/css/prettyPhoto.css" rel="stylesheet" type="text/css">
    <link href="website-assets/plugins/owl-carousel/css/owl.carousel.css" rel="stylesheet" type="text/css">
    <link href="website-assets/plugins/owl-carousel/css/owl.theme.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="website-assets/plugins/rs-plugin/css/settings.css" media="screen" />
    <!--[if lte IE 8]><link rel="stylesheet" type="text/css" href="website-assets/css/ie8.css" media="screen" /><![endif]-->
    <!-- Color Style -->
    <link href="website-assets/colors/blue.css" rel="stylesheet" type="text/css">
    <!-- SCRIPTS
      ================================================== -->
    <script src="website-assets/js/modernizr.js"></script><!-- Modernizr -->

    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            s1.src='https://embed.tawk.to/5cc5fcc34aa6620df2cf9831/default';
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
        })();
    </script>
    <!--End of Tawk.to Script-->

</head>
<body>
<div class="body footer-style2">
    @include('website.template.header')
    <div class="main" role="main">
        <div id="content" class="content full">
            @yield('page-content')
        </div>
    </div>
    @include('website.template.footer')
</div>
<script src="http://code.jquery.com/jquery-latest.min.js"></script> <!-- Jquery Library Call -->
<script>if (typeof jQuery == 'undefined') {document.write(unescape("%3Cscript src='js/jquery-2.0.0.min.js' type='text/javascript'%3E%3C/script%3E"));}</script>
<script src="website-assets/plugins/prettyphoto/js/prettyphoto.js"></script>
<script src="website-assets/plugins/owl-carousel/js/owl.carousel.min.js"></script>
<script src="website-assets/plugins/page-scroller/jquery.pagescroller.js"></script>
<script src="website-assets/js/helper-plugins.js"></script> <!-- Plugins -->
<script src="website-assets/js/bootstrap.js"></script> <!-- UI -->
<script src="website-assets/js/init.js"></script> <!-- All Scripts -->
<script src="website-assets/plugins/rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
<script src="website-assets/plugins/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script src="website-assets/js/revolution-slider-init.js"></script> <!-- Revolutions Slider Intialization -->
<!-- End Js -->
@yield('page-javascript')

@yield('import-javascript')

</body>

</html>
