<footer class="site-footer-top" id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-6">
                <section class="footer-widget" id="newsletter">
                    <h1 class="logo"> <a href="{{ route('website-home') }}"> <img src="/website-assets/images/logo/wLogo.png" alt="Site Logo" style="max-width: 50%"> </a> </h1>

                    <p>Maa Softech offers Website Designing, Web Application Development, E-commerce solutions and Online Marketing. Our professionals have proven industry and technical experience and use pioneering techniques and standard methodologies to provide innovative solutions in time. With an experience of over 3 years Maa Softech, professional Web Designing Company has become one of the foremost choices of people looking for professional web Design & Devlopment Company in Vadodara.</p>
                </section>
            </div>
            <div class="col-md-2 col-sm-6">
                <section class="footer-widget" id="twitter">
                    <h4>Quick Links</h4>
                    <ul>
                        <li><a href="javascript:void(0)">About us</a></li>
                        <li><a href="javascript:void(0)">Portfolio</a></li>
                        <li><a href="javascript:void(0)">Vision & Mission</a></li>
                        <li><a href="javascript:void(0)">News</a></li>
                        <li><a href="javascript:void(0)">Contact Us</a></li>
                    </ul>
                </section>
            </div>
            <div class="col-md-3 col-sm-6">
                <section class="footer-widget" id="flicker-widget">
                    <h4>Portfolio</h4>
                    <ul class="flickr-widget clearfix">
                        <li>
                            <a href=""> <img src="/website-assets/images/portfolio/mobile-application.jpg" class="footer-portfolio"></a>
                        </li>
                        <li>
                            <a href=""> <img src="/website-assets/images/portfolio/amaflip.png" class="footer-portfolio"></a>
                        </li>
                        <li>
                            <a href=""> <img src="/website-assets/images/portfolio/brochure.jpg" class="footer-portfolio"></a>
                        </li>
                        <li>
                            <a href=""> <img src="/website-assets/images/portfolio/ser-bro.jpg" class="footer-portfolio"></a>
                        </li>
                        <li>
                            <a href=""> <img src="/website-assets/images/portfolio/visioncare.png" class="footer-portfolio"></a>
                        </li>
                        <li>
                            <a href=""> <img src="/website-assets/images/portfolio/logo.jpg" class="footer-portfolio"></a>
                        </li>
                    </ul>
                </section>
            </div>
            <div class="col-md-3 col-sm-6">
                <section class="footer-widget" id="contact">
                    <h4>Contact Us</h4>
                    <address class="ico">
                        <p><i class="fa fa-map-marker"></i> <span><strong>Address:</strong> <br>
              </span></p>
                        <p><i class="fa fa-phone"></i> <span><strong>Phone:</strong> (+91) 9173705320 </span></p>
                        <p><i class="fa fa-phone"></i> <span><strong>Phone:</strong> (+91) 9879859202</span></p>
                        <p><i class="fa fa-phone"></i> <span><strong>Phone:</strong> (+91) 8200286513</span></p>
                        <p><i class="fa fa-envelope"></i> <span><strong>Email:</strong> maasoftechvadodara@gmail.com</span></p>
                    </address>
                </section>
            </div>
        </div>
    </div>
</footer>
<!-- End Footer -->
<footer class="site-footer-bottom">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <p>&copy; <?= date('Y')?> Maa Softech. All Rights Reserved.</p>
            </div>
            <div class="col-md-6">
                <ul class="social-icons-bar pull-right">
                    <li><a href="javascript:void(0)"><i class="fa fa-lg fa-facebook"></i></a></li>
                    <li><a href="javascript:void(0)"><i class="fa fa-lg fa-twitter"></i></a></li>
                    <li><a href="javascript:void(0)"><i class="fa fa-lg fa-google-plus"></i></a></li>
                    <li><a href="javascript:void(0)"><i class="fa fa-lg fa-youtube"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>