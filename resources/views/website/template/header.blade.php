<div class="topbar hidden-sm hidden-xs">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <nav class="secondary-menu">
                    <ul class="pull-left">
                        <li><a href="javascript:void(0)"><i class="fa fa-lg fa-twitter"></i></a></li>
                        <li><a href="javascript:void(0)"><i class="fa fa-lg fa-facebook"></i></a></li>
                        <li><a href="javascript:void(0)"><i class="fa fa-lg fa-youtube"></i></a></li>
                        <li><a href="javascript:void(0)"><i class="fa fa-lg fa-google-plus"></i></a></li>
                    </ul>
                </nav>
            </div>
            <div class="col-md-6">
                <nav class="secondary-menu">
                    <ul class="pull-right">

                        <li><a href="javascript:void(0)">Terms</a></li>
                        <li><a href="javascript:void(0)">FAQS</a></li>
                        @if(Session::get('user'))
                            <li><a href="{{ route('user-logout') }}">Logout</a></li>
                        @endif
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>
<header class="site-header" id="sticky-nav">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <h1 class="logo"> <a href="{{ route('website-home') }}"> <img src="/website-assets/images/logo/logo.png" alt="Maa Softech Logo"> </a> </h1>
            </div>
            <div class="col-md-9">
                <button class="mmenu-toggle"><i class="fa fa-bars fa-lg"></i></button>
                <nav class="main-menu">
                    <ul class="sf-menu" id="main-menu">
                        <li><a href="{{ route('website-home') }}">Home</a></li>
                        <li><a href="">About Us </a>
                        </li>
                        <li class="megamenu"><a href=""> Services <i class="fa fa-angle-down"></i></a>
                            <ul class="dropdown">
                                <li>
                                    <div class="megamenu-container container">
                                        <div class="row">
                                            <div class="col-md-3 hidden-sm hidden-xs"> <span class="megamenu-sub-title">Our Newest feature</span> <img src="/website-assets/images/home/Website-Development.jpg" alt="" class="margin-20">
                                            </div>
                                            <div class="col-md-3">
                                                <ul class="sub-menu">
                                                    <li> <span class="megamenu-sub-title">Web Startup</span>
                                                        <ul>
                                                            <li><a href="javascript:void(0)">Domain Registration</a></li>
                                                            <li><a href="javascript:void(0)">Web Hosting</a></li>
                                                        </ul>
                                                    </li>
                                                    <li> <span class="megamenu-sub-title">Designing</span>
                                                        <ul>
                                                            <li><a href="javascript:void(0)">Logo Design</a></li>
                                                            <li><a href="javascript:void(0)">Brochure</a></li>
                                                            <li><a href="javascript:void(0)">Packaging Design</a></li>
                                                        </ul>
                                                    </li>

                                                </ul>
                                            </div>
                                            <div class="col-md-3">
                                                <ul class="sub-menu">
                                                    <li> <span class="megamenu-sub-title">Digital Marketing</span>
                                                        <ul>
                                                            <li><a href="javascript:void(0)">SEO(Search Engine Optimization)</a></li>
                                                            <li><a href="javascript:void(0)">SMM(Social Media Marketing)</a></li>
                                                            <li><a href="javascript:void(0)">Google Adwords</a></li>
                                                            <li><a href="javascript:void(0)">Email Marketing</a></li>
                                                            <li><a href="javascript:void(0)">Content Marketing</a></li>
                                                        </ul>
                                                    </li>
                                                    <li> <span class="megamenu-sub-title">Mobile Application</span>
                                                        <ul>
                                                            <li><a href="javascript:void(0)">Mobile Application Development</a></li>
                                                            <li><a href="javascript:void(0)">Mobile Application Design</a></li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-md-3">
                                                <ul class="sub-menu">
                                                    <li> <span class="megamenu-sub-title">Web Development & Design</span>
                                                        <ul>
                                                            <li><a href="javascript:void(0)">Website Development</a></li>
                                                            <li><a href="javascript:void(0)">Website Maintenance</a></li>
                                                            <li><a href="javascript:void(0)">Website Design</a></li>
                                                            <li><a href="javascript:void(0)">Website Redesign</a></li>
                                                            <li><a href="javascript:void(0)">PSD To HTML</a></li>
                                                            <li><a href="javascript:void(0)">Website Animation Design</a></li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <img src="/website-assets/images/home/user-interface-design-etain.png" width="100%" style="max-height: 9rem">
                                        </div>

                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li><a href="">Portfolio</a></li>
                        <li><a href="">contact</a></li>
                        @if(!Session::get('user'))
                            <li><a href="{{ route('user-login') }}">Login</a></li>
                            <li><a href="{{ route('user-register-types') }}">Register</a></li>
                        @endif
                    </ul>
                </nav>
            </div>
        </div>
    </div>

    <nav class="mobile-menu">
        <div class="container">
            <div class="row"></div>
        </div>
    </nav>
</header>