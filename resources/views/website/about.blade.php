@extends('website.template.layout')

@section('page-title')
    About Us | Top web development company | best web development solutions | web services company | web application development | web designing company | Software development services | web development company-webserve technology
    @endsection

@section('page-content')

    <header class="page-header high parallax margin-0" style="background-image:url(/website-assets/images/banner/aboutus.jpg)">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <section>
                        <h1 class="pull-left">Something About Us</h1>
                    </section>
                </div>
            </div>
        </div>
    </header>

    <div class="padding-tb75 text-align-center lgray-color">
        <div class="container">
            <div class="row">
                <div class="col-md-12" data-appear-animation="fadeIn" data-appear-animation-delay=".2">
                    <h4>We are Maa Softech</h4>
                    <hr class="thick">
                    <div class="clearfix"></div>
                    <p class="col-md-12 big">Maa Sofotech is a Professional web designing and web development company in Vadodara, which is offers a full facility for designing a website at a reasonable price. We are proving web design and digital marketing company working since 2018, completing years in the Web designing industry providing flawless services in website development, creative logo designs, online marketing, Website redesigning techniques and even advanced classified portal development with our web development experts. It is a company where style with Innovative design and development is displayed with smart work. We focus to work with enterprises throughout the global.</p>
                    {{--<button class="btn btn-primary btn-lg moveto-sec4 margin-60">View some of our work</button>--}}
                </div>

            </div>
        </div>
    </div>

    <div class="padding-tb45 text-align-center">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-align-center">
                    <h2>Some Of Our Services</h2>
                    <h4 class="heading-hr no-strong accent-color-text margin-50"><span>Choose it experience the change</span></h4>
                </div>
            </div>
            <div class="padding-tb45">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4" data-appear-animation="bounceIn" data-appear-animation-delay="25">
                            <section class="features text-align-center"> <i class="ic-lg fa fa-code fa-2x"></i>
                                <h3 class="short">Web Development</h3>
                                <p>A web site is the front end of the business and it builds the impression of your back services and business, It is a means through which client gets the understanding about your occupation and organization. When web site is user-friendly and eye-catching giving all the information to visitors usually asks for, you can easily be successful in making first impression on the mind of the visitor.</p>
                            </section>
                        </div>
                        <div class="col-md-4" data-appear-animation="bounceIn" data-appear-animation-delay="125">
                            <section class="features text-align-center"> <i class="ic-lg fa fa-bar-chart-o fa-2x"></i>
                                <h3 class="short">Modern Graphic Design</h3>
                                <p>Corporate branding is one of the key elements of business success. This covers Logo Design, Brochure Design and Printing, Letterheads and Business Cards, Banner Ad Designs and all Graphic designing. Our graphic design process begins with a clear understanding of you and your business, ensuring that we develop ideas and solutions that strengthen the perceptions of your products and services.</p>
                            </section>
                        </div>
                        <div class="col-md-4" data-appear-animation="bounceIn" data-appear-animation-delay="225">
                            <section class="features text-align-center"> <i class="ic-lg fa fa-thumbs-o-up fa-2x"></i>
                                <h3 class="short">Social Marketing</h3>
                                <p>Social media marketing refers to the process of gaining website traffic or attention through social media sites. Social media marketing programs usually center on efforts to create content that attracts attention and encourages readers to share it with their social networks. Hence, this form of marketing is driven by word-of-mouth, meaning it results in earned media rather than paid media.</p>
                            </section>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-md-4" data-appear-animation="bounceIn" data-appear-animation-delay="225">
                            <section class="features text-align-center"> <i class="ic-lg fa fa-mobile fa-2x"></i>
                                <h3 class="short">Mobile App Development</h3>
                                <p>Mobile app development is the act or process by which a mobile app is developed for mobile devices, such as personal digital assistants, enterprise digital assistants or mobile phones. ... Mobile app development has been steadily growing, in revenues and jobs created.</p>
                            </section>
                        </div>
                        <div class="col-md-4" data-appear-animation="bounceIn" data-appear-animation-delay="225">
                            <section class="features text-align-center"> <i class="ic-lg fa fa-pencil fa-2x"></i>
                                <h3 class="short">Website Design</h3>
                                <p>Web design encompasses many different skills and disciplines in the production and maintenance of websites. The different areas of web design include web graphic design; interface design; authoring, including standardised code and proprietary software; user experience design; and search engine optimization.</p>
                            </section>
                        </div>
                        <div class="col-md-4" data-appear-animation="bounceIn" data-appear-animation-delay="225">
                            <section class="features text-align-center"> <i class="ic-lg fa fa-globe fa-2x"></i>
                                <h3 class="short">Search Engine Optimization (SEO)</h3>
                                <p>SEO  Quality of traffic. You can attract all the visitors in the world, but if they're coming to your site because Google tells them you're a resource for Apple computers when really you're a farmer selling apples, that is not quality traffic. Instead you want to attract visitors who are genuinely interested in products that you offer.</p>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container margin-50">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <h4><span>How We Do It.</span></h4>
                <ul class="hearts">
                    <li>We have a keen understanding of people, processes and technology. We keep abreast of emerging trends and evolving business practices.</li>
                    <li>Our team are trained to understand, anticipate and respond to your needs / queries intelligently.</li>
                    <li>We believe in delivering a business solution and not an IT solution.</li>
                </ul>
                <h4><span>Our Mission</span></h4>
                <ul class="hearts">
                    <li> At Maa Softech, we have a mission to ensure that we help our clients achieve their business objective efficiently. We make sure that our client's get cost effective solutions for their business needs.</li>
                </ul>
                <h4><span>Our Vision</span></h4>
                <ul class="hearts">
                    <li> For our client's satisfaction, we always emphasis on availability, affordability and accessibility of our services accompanied by our passion for innovation and prolonged services.</li>
                    <li>  We also have a vision of imparting technical skills to young and inexperienced IT graduates on a voluntary basis so that they can easily launch their career in IT.</li>
                </ul>
            </div>
            <div class="col-md-6 col-sm-6"> <img src="/website-assets/images/home/hand-drawn-business.jpg" alt=""> </div>
        </div>
    </div>

    <div class="padding-tb45 lgray-color text-align-center">
        <div class="container">
            <div class="row">
                <div class="col-md-12 appear-animation fadeIn appear-animation-visible" data-appear-animation="fadeIn" data-appear-animation-delay=".2">
                    <h1 class="no-strong"><span class="accent-color-text">Maa Softech -:</span> Your Business, We make It Simple and Faster</h1>
                    <a href="javascript:void(0)" class="btn btn-lg btn-primary">Contact Us Now!</a> </div>
            </div>
        </div>
    </div>
@endsection()