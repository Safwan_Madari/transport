@extends('website.template.layout')

@section('page-title','Contact Us')

@section('page-content')

<header class="page-header high parallax margin-0" style="background-image:url(/website-assets/images/banner/contact-us.jpg)">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <section>
                    <h1 class="pull-left">Contact Us</h1>
                </section>
            </div>
        </div>
    </div>
</header>

<div class="container">
    <div class="row mt-3">
        <div class="col-md-7">
            <h2>Get in touch <strong>with us</strong></h2>
            <p>To Get Best Service Get In Touch Today.</p>
            @if (session('errors'))
            <div class="alert alert-danger">
                <ul>
                    @foreach (session('errors')->all() as $error)
                    <li><i class="fa fa-hand-o-right"></i> {{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            @if (session('success'))
            <div class="alert alert-success"> {{ session('success') }}</div>
            @endif
            <form method="post" class="contact-form">
                {{ csrf_field() }}
                <div class="row">
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6">
                            <input type="text" name="name"  class="form-control input-lg" placeholder="Name*">
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <input type="email" name="email"  class="form-control input-lg" placeholder="Email*">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <div class="col-md-6">
                            <input type="text" name="mobile" class="form-control input-lg" placeholder="Mobile No">
                        </div>
                        <div class="col-md-6">
                            <input type="text" name="subject" class="form-control input-lg" placeholder="Subject">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <div class="col-md-12">
                            <textarea cols="8" rows="6" name="message" class="form-control input-lg" placeholder="Message"></textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <div class="col-md-12">
                            <button class="btn btn-primary btn-lg">Submit</button>
                        </div>
                    </div>
                </div>
            </form>

        </div>
        <div class="col-md-5 sidebar right-sidebar">
            {{--<div class="w-map push-top">--}}
                {{--<div class="w-map-h clearfix" style="height: 250px;"> </div>--}}
            {{--</div>--}}
            <h3 class="push-top">Get Social</h3>
            <ul class="social-icons-list">
                <li><a href="#"><i class="fa fa-facebook-square fa-3x"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter-square fa-3x"></i></a></li>
                <li><a href="#"><i class="fa fa-linkedin-square fa-3x"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus-square fa-3x"></i></a></li>
                <li><a href="#"><i class="fa fa-youtube-square fa-3x"></i></a></li>
            </ul>
            <h3>Our Location</h3>
            <ul>
                <li class="mb-1"><strong><i class="fa fa-map-marker"></i> Address : </strong> Vadodara, Gujrat, India</li>
                <li class="mb-1"><strong><i class="fa fa-phone"></i> Phone : </strong> (+91) 9173705320</li>
                <li class="mb-1"><strong><i class="fa fa-phone"></i> Phone : </strong> (+91) 9879859202</li>
                <li class="mb-1"><strong><i class="fa fa-phone"></i> Phone : </strong> (+91) 8200286513</li>
                <li class="mb-1"><strong><i class="fa fa-envelope"></i> Email : </strong> <a href="mailto:maasoftechvadodara@gmail.com
">maasoftechvadodara@gmail.com
                    </a></li>
            </ul>
        </div>
    </div>
</div>

@endsection()