@extends('website.template.layout')

@section('page-title','portfolio')

@section('page-content')

<header class="page-header parallax" style="background-image:url(/website-assets/images/banner/business-plan.jpg)">
	<section>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1>Portfolio</h1>
					<p>Our Work Say Not We.</p>
				</div>
			</div>
		</div>
	</section>
</header>


<div class="container margin-40">
	<ul class="nav nav-pills sort-source" data-sort-id="portfolio" data-option-key="filter">
		<li data-option-value="*" class="active"><a href="#">Show All</a></li>
		<li data-option-value=".E-commarce"><a href="#">E-Commarce</a></li>
		<li data-option-value=".Dinamic"><a href="#">Websites</a></li>
		<li data-option-value=".mobile-app"><a href="#">Mobile Application</a></li>
		<li data-option-value=".Graphic-Design"><a href="#">Graphics Designs</a></li>
		<li data-option-value=".logo-Design"><a href="#">logo Designs</a></li>

	</ul>
	<hr />
	<div class="row">
		<ul class="portfolio-list sort-destination" data-sort-id="portfolio">     
			<li class="col-md-4 isotope-item E-commarce"> 
				<!-- Portfolio Item  -->
				<div class="portfolio-item">
					<div class="portfolio-image"> <img src="/website-assets/images/portfolio/amaflip.png" alt="Portfolio 1"> </div> 
					<span class="project -overlay"> 
						<span class="project-info">
							<span class="action-icons">
								<a title="Enlarge" data-rel="prettyPhoto[galname]" href="/website-assets/images/portfolio/amaflip.png">
									<i class="fa fa-plus fa-2x"></i>
								</a>
							</span>
							<span class="project-name">Ecommarce Website</span>
						</span>
					</span> 
				</div>
			</li>

			<li class="col-md-4 isotope-item Dinamic"> 
				<!-- Portfolio Item  -->
				<div class="portfolio-item">
					<div class="portfolio-image"> <img src="/website-assets/images/portfolio/visioncare.png" alt="Portfolio 1"> </div> 
					<span class="project -overlay"> 
						<span class="project-info">
							<span class="action-icons">
								<a title="Enlarge" data-rel="prettyPhoto[galname]" href="/website-assets/images/portfolio/visioncare.png">
									<i class="fa fa-plus fa-2x"></i>
								</a>
							</span>
							<span class="project-name">Marketing Website</span>
						</span>
					</span> 
				</div>
			</li>

			<li class="col-md-4 isotope-item Graphic-Design"> 
				<!-- Portfolio Item  -->
				<div class="portfolio-item">
					<div class="portfolio-image"> <img src="/website-assets/images/portfolio/brochure.jpg" alt="Portfolio 1"> </div> 
					<span class="project -overlay"> 
						<span class="project-info">
							<span class="action-icons">
								<a title="Enlarge" data-rel="prettyPhoto[galname]" href="/website-assets/images/portfolio/brochure.jpg">
									<i class="fa fa-plus fa-2x"></i>
								</a>
							</span>
							<span class="project-name">Graphic Design</span>
						</span>
					</span> 
				</div>
			</li>

			<li class="col-md-4 isotope-item Graphic-Design"> 
				<!-- Portfolio Item  -->
				<div class="portfolio-item">
					<div class="portfolio-image"> <img src="/website-assets/images/portfolio/ser-bro.jpg" alt="Portfolio 1"> </div> 
					<span class="project -overlay"> 
						<span class="project-info">
							<span class="action-icons">
								<a title="Enlarge" data-rel="prettyPhoto[galname]" href="/website-assets/images/portfolio/ser-bro.jpg">
									<i class="fa fa-plus fa-2x"></i>
								</a>
							</span>
							<span class="project-name">Graphic Design</span>
						</span>
					</span> 
				</div>
			</li>

			<li class="col-md-4 isotope-item mobile-app"> 
				<!-- Portfolio Item  -->
				<div class="portfolio-item">
					<div class="portfolio-image"> <img src="/website-assets/images/portfolio/mobile-application.jpg" alt="Portfolio 1"> </div> 
					<span class="project -overlay"> 
						<span class="project-info">
							<span class="action-icons">
								<a title="Enlarge" data-rel="prettyPhoto[galname]" href="/website-assets/images/portfolio/mobile-application.jpg">
									<i class="fa fa-plus fa-2x"></i>
								</a>
							</span>
							<span class="project-name">Mobile Application</span>
						</span>
					</span> 
				</div>
			</li>

			<li class="col-md-4 isotope-item logo-Design"> 
				<!-- Portfolio Item  -->
				<div class="portfolio-item">
					<div class="portfolio-image"> <img src="/website-assets/images/portfolio/logo.jpg" alt="Portfolio 1" style="max-width: 70%"> </div> 
					<span class="project -overlay"> 
						<span class="project-info">
							<span class="action-icons">
								<a title="Enlarge" data-rel="prettyPhoto[galname]" href="/website-assets/images/portfolio/logo.jpg">
									<i class="fa fa-plus fa-2x"></i>
								</a>
							</span>
							<span class="project-name">Mobile Application</span>
						</span>
					</span> 
				</div>
			</li>



		</ul>
	</div>
</div>

@endsection()