@extends('admin.template.layout')

@section('title')
    Admin Manager
@endsection

@section('page-content')

    <div class="page-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="float-right page-breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin-dashboard') }}">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('admin-manager-create') }}">Admin Manager</a></li>
                        </ol>
                    </div>
                    <h5 class="page-title">Admin Manager</h5>
                </div>
            </div>
            <!-- end row -->
            <div class="card p-2 mb-4">
                <form action="" method="get">
                    <div class="row">
                        <div class="col-md-3">
                            <input type="text" name="search" class="form-control" placeholder="Search" value="{{ Request::get('search') }}">
                        </div>
                        <div class="col-md-3">
                            <a href="" class="btn btn-success btn-sm">Search</a>
                        </div>
                    </div>
                </form>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card m-b-30">
                        <div class="card-body">
                            <div class="table-rep-plugin">
                                <div class="table-responsive b-0" data-pattern="priority-columns">
                                    <table id="tech-companies-1" class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Mobile</th>
                                            <th>Username</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($admins as $index => $admin)
                                            <tr>
                                                <td>{{ $index+1 }}</td>
                                                <td>{{ $admin->full_name }}</td>
                                                <td>{{ $admin->mobile }}</td>
                                                <td>{{ $admin->username }}</td>
                                                <td>
                                                    @if($admin->status == 1)
                                                        <span class="badge badge-success">Active</span>
                                                    @else
                                                        <span class="badge badge-danger">In-Active</span>
                                                    @endif
                                                </td>
                                                <td>
                                                    <a href="{{ route('admin-manager-update',[ 'id' => $admin->id]) }}" class="btn btn-primary btn-sm">Update</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    {{ $admins->appends(['search' => Request::get('search')])->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->
        </div>
        <!-- container fluid -->
    </div>

@endsection