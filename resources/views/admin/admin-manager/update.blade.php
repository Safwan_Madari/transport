@extends('admin.template.layout')

@section('title')
    Update {{ $admin->full_name }} Details
@endsection

@section('page-content')

    <div class="page-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="float-right page-breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin-dashboard') }}">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('admin-manager-create') }}">Admin Manager</a></li>
                            <li class="breadcrumb-item active">Update </li>
                        </ol>
                    </div>
                    <h5 class="page-title">Update {{ $admin->full_name }} Details</h5>
                </div>
            </div>
            <!-- end row -->
            <div class="row">
                <div class="col-lg-6 offset-3">
                    <div class="card m-b-30">
                        <div class="card-body">
                            <h4 class="mt-0 mb-2 header-title text-danger ">All * Marked Field Are Required</h4>

                            <form  method="post">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label>First Name</label>
                                    <div><input type="text" name="first_name" class="form-control" required placeholder="Enter First Name" value="{{ $admin->first_name }}"></div>
                                </div>
                                <div class="form-group">
                                    <label>Last Name</label>
                                    <div><input type="text" name="last_name" class="form-control" required placeholder="Enter Last Name"
                                        value="{{ $admin->last_name }}"></div>
                                </div>
                                <div class="form-group">
                                    <label>Mobile</label>
                                    <div><input type="number" name="mobile" class="form-control" required placeholder="Enter Mobile Number" value="{{ $admin->mobile }}"></div>
                                </div>

                                <div class="form-group">
                                    <label>Username</label>
                                    <div><input type="text" name="username" class="form-control" required placeholder="Enter Username"
                                        value="{{ $admin->username }}"></div>
                                </div>

                                <div class="form-group">
                                    <label>Status</label>
                                    <div>
                                        <select name="status" id="" class="form-control">
                                            <option value="1"{{ $admin->status == 1 ? 'selected' : null }}>Active</option>
                                            <option value="2"{{ $admin->status == 2 ? 'selected' : null }}>In-Active</option>
                                        </select>
                                    </div>
                                </div>

                                {{--<div class="form-group">--}}
                                    {{--<label>Password</label>--}}
                                    {{--<div><input type="text" name="password" class="form-control" required readonly value="{{ $admin->password }}"></div>--}}
                                {{--</div>--}}


                                <button class="btn btn-block btn-secondary">Update</button>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
            <!-- end row -->
        </div>
        <!-- container fluid -->
    </div>

@endsection