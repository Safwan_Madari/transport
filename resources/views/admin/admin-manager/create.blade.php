@extends('admin.template.layout')

@section('title')
    Create Admin
@endsection

@section('page-content')

    <div class="page-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="float-right page-breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin-dashboard') }}">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('admin-manager-view') }}">Admin Manager</a></li>
                            <li class="breadcrumb-item active">Create</li>
                        </ol>
                    </div>
                    <h5 class="page-title">Create New Admin</h5>
                </div>
            </div>
            <!-- end row -->
            <div class="row">
                <div class="col-lg-6 offset-3">
                    <div class="card m-b-30">
                        <div class="card-body">
                            <h4 class="mt-0 mb-2 header-title text-danger ">All * Marked Field Are Required</h4>

                            <form  method="post">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label>First Name</label>
                                    <div><input type="text" name="first_name" class="form-control" required placeholder="Enter First Name"></div>
                                </div>
                                <div class="form-group">
                                    <label>Last Name</label>
                                    <div><input type="text" name="last_name" class="form-control" required placeholder="Enter Last Name"></div>
                                </div>
                                <div class="form-group">
                                    <label>Mobile</label>
                                    <div><input type="number" name="mobile" class="form-control" required placeholder="Enter Mobile Number"></div>
                                </div>

                                <div class="form-group">
                                    <label>Username</label>
                                    <div><input type="text" name="username" class="form-control" required placeholder="Enter Username"></div>
                                </div>

                                <div class="form-group">
                                    <label>Password</label>
                                    <div><input type="text" name="password" class="form-control" required placeholder="Enter Password"></div>
                                </div>
                                <div class="form-group">
                                    <label>Confirm Password</label>
                                    <div><input type="text" name="confirm_password" class="form-control" required placeholder="Confirm Password"></div>
                                </div>

                                <button class="btn btn-block btn-secondary">Create</button>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
            <!-- end row -->
        </div>
        <!-- container fluid -->
    </div>

@endsection