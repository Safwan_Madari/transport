<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimal-ui">
    <title>{{ env("COMPANY_NAME") }}</title>
    <meta content="Admin Dashboard" name="description">
    <meta content="ThemeDesign" name="author">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" href="/admin-assets/images/favicon.ico">
    <link href="/admin-assets/plugins/sweet-alert2/sweetalert2.min.css" rel="stylesheet" type="text/css">
    <link href="/admin-assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="/admin-assets/css/icons.css" rel="stylesheet" type="text/css">
    <link href="/admin-assets/css/style.css" rel="stylesheet" type="text/css">
</head>
<body class="fixed-left">
<!-- Loader -->
{{--<div id="preloader">--}}
{{--<div id="status">--}}
{{--<div class="spinner"></div>--}}
{{--</div>--}}
{{--</div>--}}
<!-- Begin page -->
<div class="accountbg">
    <div class="content-center">
        <div class="content-desc-center">
            <div class="container">
                <div class="row justify-content-center">

                    <div class="col-lg-5 col-md-8">
                        <div class="card">
                            <div class="card-body">
                                <h3 class="text-center mt-0 m-b-15"><a href="{{ route('admin-login') }}" class="logo logo-admin"><img src="/website-assets/images/logo/logo.png" height="60" alt="logo"></a></h3>
                                <h4 class="text-muted text-center font-18"><b>Sign In</b></h4>
                                <div class="p-2">
                                    <form class="form-horizontal m-t-20" method="post">
                                        {{ csrf_field() }}
                                        <div class="form-group row">
                                            <div class="col-12">
                                                <input class="form-control" type="text" name="username" placeholder="Username">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-12">
                                                <input class="form-control" type="password" name="password" placeholder="Password">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-12">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="customCheck1">
                                                    <label class="custom-control-label" for="customCheck1">Remember me</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group text-center row m-t-20">
                                            <div class="col-12">
                                                <button class="btn btn-primary btn-block waves-effect waves-light" >Log In</button></div>`
                                        </div>
                                        <div class="form-group m-t-10 mb-0 row">
                                            <div class="col-sm-7 m-t-20">
                                                <a href="javascript:void(0)" class="text-muted">
                                                    <i class="mdi mdi-lock"></i> Forgot your password?</a>
                                            </div>
                                            <div class="col-sm-5 m-t-20">
                                                <a href="javascript:void(0) " class="text-muted">
                                                    <i class="mdi mdi-account-circle"></i> Create an account</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end row -->
            </div>
        </div>
    </div>
</div>
<!-- jQuery  -->
<script src="/admin-assets/js/jquery.min.js"></script>
<script src="/admin-assets/js/bootstrap.bundle.min.js"></script>
<script src="/admin-assets/js/modernizr.min.js"></script>
<script src="/admin-assets/js/detect.js"></script>
<script src="/admin-assets/js/fastclick.js"></script>
<script src="/admin-assets/js/jquery.slimscroll.js">

</script><script src="/admin-assets/js/jquery.blockUI.js">

</script><script src="/admin-assets/js/waves.js"></script>
<script src="/admin-assets/js/jquery.nicescroll.js"></script>
<script src="/admin-assets/js/jquery.scrollTo.min.js"></script>
<!-- App js -->
<script src="/admin-assets/js/app.js"></script>
{{--sweet alert --}}
<!-- Sweet-Alert  -->
<script src="/admin-assets/plugins/sweet-alert2/sweetalert2.min.js"></script>


<script>
    @if(session('errors'))
swal('Oops', '{{ session('errors')->first() }}', 'error');
    @elseif(session('error'))
swal('Oops', '{{ session('error') }}', 'error');
    @elseif(session('success'))
swal('Hurray', '{{ session('success') }}', 'success');
    @endif
</script>
</body>
</html>