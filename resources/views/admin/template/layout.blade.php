<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimal-ui">
    <title>@yield('title')| {{ env('COMPANY_NAME') }}</title>
    <meta content="Admin Dashboard" name="description">
    <meta content="ThemeDesign" name="author">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" href="/admin-assets/images/favicon.ico">
    <!--Morris Chart CSS -->
    <link rel="stylesheet" href="/admin-assets/plugins/morris/morris.css">
    <link href="/admin-assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="/admin-assets/css/icons.css" rel="stylesheet" type="text/css">
    <link href="/admin-assets/css/style.css" rel="stylesheet" type="text/css">
    <link href="/admin-assets/plugins/sweet-alert2/sweetalert2.min.css" rel="stylesheet" type="text/css">
    @yield('page-css')
    @yield('import-css')
</head>

<body class="fixed-left">
<!-- Loader -->
{{--<div id="preloader">--}}
{{--<div id="status">--}}
{{--<div class="spinner"></div>--}}
{{--</div>--}}
{{--</div>--}}
<div id="wrapper">
    @include('admin.template.left-sidebar')
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <!-- Top Bar Start -->
            @include('admin.template.header')

            @yield('page-content')

        </div>
        @include('admin.template.footer')
    </div>

</div>

@yield('page-js')
@yield('import-js')
<script src="/admin-assets/js/jquery.min.js"></script>
<script src="/admin-assets/js/bootstrap.bundle.min.js"></script>
<script src="/admin-assets/js/modernizr.min.js"></script>
<script src="/admin-assets/js/detect.js"></script>
<script src="/admin-assets/js/fastclick.js"></script>
<script src="/admin-assets/js/jquery.slimscroll.js"></script>
<script src="/admin-assets/js/jquery.blockUI.js"></script>
<script src="/admin-assets/js/waves.js"></script>
<script src="/admin-assets/js/jquery.nicescroll.js"></script>
<script src="/admin-assets/js/jquery.scrollTo.min.js"></script>
<!-- skycons -->
<script src="/admin-assets/plugins/skycons/skycons.min.js"></script>
<!-- skycons -->
<script src="/admin-assets/plugins/peity/jquery.peity.min.js"></script>
<!--Morris Chart-->
<script src="/admin-assets/plugins/morris/morris.min.js"></script>
<script src="/admin-assets/plugins/raphael/raphael-min.js"></script>
<!-- dashboard -->
<script src="/admin-assets/pages/dashboard.js"></script>
<!-- App js -->
<script src="/admin-assets/js/app.js"></script>

<!-- Sweet-Alert  -->
<script src="/admin-assets/plugins/sweet-alert2/sweetalert2.min.js"></script>

<script>
    @if(session('errors'))
swal('Oops', '{{ session('errors')->first() }}', 'error');
    @elseif(session('error'))
swal('Oops', '{{ session('error') }}', 'error');
    @elseif(session('success'))
swal('Hurray', '{{ session('success') }}', 'success');
    @endif
</script>
</body>
</html>
