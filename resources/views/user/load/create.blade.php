@extends('user.template.layout')

@section('title','Post New Load')

@section('content')

    <div class="content-header row">
        <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">Post Load</h3>
        </div>
        <div class="content-header-right col-md-8 col-12">
            <div class="breadcrumbs-top float-md-right">
                <div class="breadcrumb-wrapper mr-1">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('user-dashboard') }}">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Post Load</a>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body" id="postLoad">
        <section id="form-control-repeater">
            <div class="row">
                <div class="col-xl-6 col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title" id="file-repeater">POST YOUR LOAD</h4>
                            <a class="heading-elements-toggle">
                                <i class="la la-ellipsis-h font-medium-3"></i>
                            </a>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">

                                <form class="form row">
                                    <div class="form-group col-md-6 mb-2">
                                        <label for="priority">Source City</label>
                                        <input type="text" class="form-control" placeholder="Enter Source City" name="origin" v-model.lazy="origin">
                                    </div>
                                    <div class="form-group col-md-6 mb-2">
                                        <label for="priority">Destination City</label>
                                        <input type="text" class="form-control" placeholder="Enter Destination City" name="destination" v-model.lazy="destination">

                                    </div>
                                    <div class="form-group col-md-6 mb-2">
                                        <label for="qty">Required Vehicle</label>
                                        <input type="number" class="form-control" placeholder="Required Vehicle" name="qty">
                                    </div>
                                    <div class="form-group col-md-6 mb-2">
                                        <label for="material_id">Select Material Type</label>
                                        <select class="form-control" name="material_id">
                                            <option>-Select-</option>
                                            @foreach($materials as $material)
                                                <option value="{{ $material->id }}">{{ $material->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6 mb-2">
                                        <label for="vehicle_id">Select Vehicle Type</label>
                                        <select class="form-control" name="vehicle_id">
                                            <option>-Select-</option>
                                            @foreach($vehicles as $vehicle)
                                                <option value="{{ $vehicle->id }}">{{ $vehicle->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6 mb-2">
                                    </div>
                                    <div class="form-group col-md-12 mb-2">
                                        <label for="weight">Weight In Tons</label>
                                        <input type="text" class="form-control" name="weight" placeholder="Weight In Tons">
                                    </div>
                                    <div class="form-group col-md-6 mb-2">
                                        <label for="priority">Scheduled Date</label>
                                        <input type="date"  name="scheduled_date" class="form-control">
                                    </div>
                                    <div class="form-group col-md-6 mb-2">
                                        <label for="priority">Scheduled Time</label>
                                        <input type="datetime"  name="scheduled_time" class="form-control">
                                    </div>
                                    <div class="form-group col-md-6 mb-2">
                                        <label for="price_on">Your Offered Freight (Price)</label>
                                        <select class="form-control" name="price_on">
                                            <option>-Select-</option>
                                            <option value="1">Per Ton</option>
                                            <option  value="2">Full Truck</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6 mb-2">
                                        <label for="price">Offering Price</label>
                                        <input type="text" class="form-control" placeholder="Offering Price" name="price">
                                    </div>
                                    <div class="form-group col-md-12 mb-2">
                                        <div class="form-group">
                                            <label for="instructions">Special Instructions</label>
                                            <textarea rows="5" class="form-control" name="instructions" placeholder="Special Instructions"></textarea>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-6 col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title" id="tel-repeater">Basic Example</h4>
                            <a class="heading-elements-toggle">
                                <i class="la la-ellipsis-h font-medium-3"></i>
                            </a>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                {{--<div>--}}
                                {{--      <input id="city" placeholder="Enter your address" onFocus="citySuggestion()" type="text"/>--}}
                                {{--    </div>--}}

                                <iframe width="100%" height="500px" frameborder="0" style="border:0" v-bind:src="src()"></iframe>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('page-javascript')
    <script src="/user-assets/plugins/vue/{{ env('APP_ENV') == 'local' ? 'vue.js' : 'vue.min.js' }}"></script>
    <script src="/user-assets/plugins/vue/vue-global.js"></script>
    <script>
        new Vue({
            el:"#postLoad",
            data:{
                origin:null,
                destination:null,
                url: "https://www.google.com/maps/embed/v1/directions?"
            },
            methods:{
                src: function () {
                    return this.url+"&origin="+this.origin+"&destination="+this.destination+"&key={{ env('GOOGLE_MAP_EMBED_API') }}";
                }
            }
        })
    </script>
@endsection