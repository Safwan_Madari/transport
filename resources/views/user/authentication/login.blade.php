@extends('user.template.registration.layout')

@section('page-title')
    Login
@endsection
@section('page-content')

    <header class="page-header text-align-center short parallax" style="background-image:url(/website-assets/images/img4.jpg)">
        <section>
            <h1>Login</h1>
        </section>
    </header>


    <div class="container" id="loadProvider">
        <form method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-7 ">
                    <img  src="/website-assets/images/banner/login-bg2.jpg" class="image-responsive">
                </div>
                <div class="col-md-5 heading-fs-bg accent-color">
                    <img src="/user-assets/images/icons/businessman.svg" alt="" class="circle-img">
                    <h3 class="heading-hr no-strong "><span><strong>Welcome to {{ env('COMPANY_NAME') }} </strong></span></h3>
                    <p class="lead capitalize mt-1 text-white">Member Login</p>
                    @if (session('errors'))
                        <div class="row">
                            <div class="col-md-12">
                                <div class="alert alert-danger" style="border-radius: 3rem;">
                                    <ul class="list">
                                        @foreach (session('errors')->all() as $error)
                                            <li><i class="fa fa-circle-o text-danger"></i> {{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-md-12">

                            <div class="form-group">
                                <input type="text" name="mobile" class="form-control" maxlength="10" placeholder="Mobile" value="{{ old('mobile') }}">
                            </div>
                            <div class="form-group">
                                <input type="password" name="password" class="form-control" placeholder="Password">
                            </div>
                            <button class="btn btn-default btn-sm btn-md mb-2 mt-1 text-center"><i class="fa fa-key"></i> Login</button>
                        </div>

                        <div class="col-md-6">
                        </div>
                        <div class="col-md-6">
                            <a href="" class="text-white pull-right mb-3">Forgot Password?</a>
                        </div>
                        <div class="col-md-12">
                            <span class="text-white ">{{ env('COMPANY_NAME')}}  may send the alerts to the mobile phone number provided by their users. </span>
                        </div>
                    </div>
                </div>



            </div>

        </form>
    </div>

@endsection




