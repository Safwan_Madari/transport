@extends('user.template.registration.layout')

@section('page-title','registration types')

@section('page-content')

    <header class="page-header text-align-center short parallax" style="background-image:url(website-assets/images/img4.jpg)">
        <section>
            <h1 >Registration Types</h1>
        </section>
    </header>

    <div class=" text-align-center">
        <div class="container">
            <div class="row">

                <div class="col-md-12 text-align-center">
                    <h2 class="margin-40 thin accent-color-text">Select Registration Types </h2>
                </div>
                <div class="col-md-3 text-align-center">
                    <img src="/website-assets/images/gif/load-provider.gif" class="gray-color-text margin-20">
                    <h4>Load Provider</h4>
                    <p class="gray-color-text">Easy to get Best Vehicle for our Good.</p>
                    <a href="{{ route('load-provider-register') }}" class="btn btn-primary btn-sm moveto-sec4 margin-60"> Register</a>
                </div>
                <div class="col-md-3 text-align-center">
                    <img src="/website-assets/images/gif/singleTruck.gif" class="gray-color-text margin-20">
                    <h4>Single Truck Registration</h4>
                    <p class="gray-color-text">Single Truck Registration</p>
                    <a href="javascript:void(0)" class="btn btn-primary btn-sm moveto-sec4 margin-60"> Register</a>
                </div>
                <div class="col-md-3 text-align-center">
                    <img src="/website-assets/images/gif/multi-truck.gif" class="gray-color-text margin-20">
                    <h4>Truck Owner Registration (Multiple Trucks)</h4>
                    <p class="gray-color-text">Truck Owner Registration (Multiple Trucks)</p>
                    <a href="javascript:void(0)" class="btn btn-primary btn-sm moveto-sec4 margin-60"> Register</a>

                </div>
                <div class="col-md-3 text-align-center">
                    <img src="/website-assets/images/gif/tranceport-company.png" class="gray-color-text margin-20">
                    <h4>Transport Company Registration</h4>
                    <p class="gray-color-text">Transport Company Registration</p>
                    <a href="javascript:void(0)" class="btn btn-primary btn-sm moveto-sec4 margin-60"> Register</a>

                </div>
            </div>
        </div>
    </div>

@endsection