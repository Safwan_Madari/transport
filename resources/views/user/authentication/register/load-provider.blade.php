@extends('user.template.registration.layout')

@section('page-title')
    load provider registration
@endsection
@section('page-content')

    <header class="page-header text-align-center short parallax" style="background-image:url(/website-assets/images/img4.jpg)">
        <section>
            <h1 >Load Provider</h1>
        </section>
    </header>


    <div class="container" id="loadProvider">
        <form method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-8">
                    <h2><strong>Sign up as Load Provider</strong></h2>
                    <p class="lead capitalize">Here you Will get best and Faster service form our best Transport Partners.</p>
                    @if (session('errors'))
                        <div class="row">
                            <div class="col-md-12">
                                <div class="alert alert-danger" style="border-radius: 3rem;">
                                    <ul class="list">
                                        @foreach (session('errors')->all() as $error)
                                            <li><i class="fa fa-circle-o text-danger"></i> {{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endif
                    <span class="badge badge-success mb-2">CONTACT PERSON INFORMATION</span>
                    <div class="row mb-3">
                        <div class="col-md-12">
                            <select name="type" class="form-control input-lg" id="type" @change="registerType($event)">
                                <option >Select Type</option>
                                <option value="1" {{ old('type') == 1 ? 'selected' : null }}>Individual</option>
                                <option value="2" {{ old('type') == 2 ? 'selected' : null }}>Company</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" name="first_name" class="form-control input-lg" placeholder="First Name" value="{{ old('first_name') }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" name="last_name" class="form-control input-lg" placeholder="Last Name" value="{{ old('last_name') }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="number" name="mobile" class="form-control input-lg" placeholder="Mobile" value="{{ old('mobile') }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" name="email" class="form-control input-lg" placeholder="Email" value="{{ old('email') }}">
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="file" name="profile_image" id="profileImage">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="password" name="password" class="form-control input-lg" placeholder="Password">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="password" name="confirm_password" class="form-control input-lg" placeholder="Retype Password">
                            </div>
                        </div>
                    </div>
                    <span class="badge badge-success mb-2">ADDRESS INFORMATION</span>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <textarea class="form-control input-lg" placeholder="Address" name="address" rows="5" cols="5" value="{{ old('address') }}"> </textarea>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <select name="state_id" id="" class="form-control">
                                    @foreach($states as $state)
                                        <option value="{{ $state->id }}">{{ $state->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" name="city" class="form-control input-lg" placeholder="City" value="{{ old('city') }}">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" name="pincode" class="form-control input-lg" placeholder="Pincode" value="{{ old('pincode') }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" name="pan_no" class="form-control input-lg" placeholder="Pancard No" value="{{ old('pan_no') }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" name="alternate_contact_person" class="form-control input-lg" placeholder="Alternate Contact Person" value="{{ old('alternate_contact_person') }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" name="alternate_mobile" class="form-control input-lg" placeholder="Alternate Mobile" value="{{ old('alternate_mobile') }}">
                            </div>
                        </div>
                    </div>
                    <div v-if="type == 2">
                        <span class="badge badge-success mb-2">COMPANY INFORMATION</span>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" name="company_name" class="form-control input-lg" placeholder="Company Name" value="{{ old('company_name') }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" name="company_type" class="form-control input-lg" placeholder="Company Type" value="{{ old('company_type') }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" name="service_tax_no" class="form-control input-lg" placeholder="Service Tax No" value="{{ old('service_tax_no') }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" name="std_code" class="form-control input-lg" placeholder="STD Code" value="{{ old('std_code') }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" name="landline_no" class="form-control input-lg" placeholder="Landline No" value="{{ old('landline_no') }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" name="gst_no" class="form-control input-lg" placeholder="GST No" value="{{ old('gst_no') }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" name="company_pan_no" class="form-control input-lg" placeholder="Company Pancard No" value="{{ old('company_pan_no') }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" name="website" class="form-control input-lg" placeholder="Company Website"
                                           value="{{ old('website') }}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 sidebar right-sidebar">
                    <div class="widget sidebar-widget">
                        <h4>Join Us</h4>
                        <img alt="img" src="/website-assets/images/banner/load-provider.png">
                    </div>
                    <section class="widget sidebar-widget" id="text-widget1">
                        <h4>Why We..?</h4>
                        <p>We Are India Largest Transport Network.</p>
                        <p>We are Providing Best Vehicle to you.</p>
                        {{--<p>We Providing Live Tracking of Vehicle .</p>--}}

                    </section>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <p class="text-danger">By clicking "Register", you certify that you are agree to our Privacy Policy and Terms of Service.</p>
                    <button class="btn btn-primary btn-md mb-2 text-center">Register</button>
                </div>
            </div>
        </form>
    </div>

@endsection


@section('page-javascript')

    <script>
        new Vue({
            el:'#loadProvider',
            data:{
                type:""
            },
            methods: {
                registerType(event) {
                    if (event.target.value == 1){
                        this.type = 1
                    }
                    else {
                        this.type = 2;
                    }
                }
            }
        });

        $('#profileImage').filer({
            limit: 1,
            maxSize: 2,
            extensions: ["jpg", "png", "gif"],
            showThumbs: true,
            changeInput: true,

        });
    </script>
@endsection


