<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="author" content="Tymk Softwares">
    <title>@yield('title') - {{ env('COMPANY_NAME') }}</title>
    <link rel="apple-touch-icon" href="/user-assets/images/company/favicon.png">
    <link rel="shortcut icon" type="image/x-icon" href="/user-assets/images/company/favicon.png">
    <link href="https://fonts.googleapis.com/css?family=Muli:300,300i,400,400i,600,600i,700,700i%7CComfortaa:300,400,700" rel="stylesheet">
    <link href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome.min.css" rel="stylesheet">
    <!-- BEGIN CSS-->
    <link rel="stylesheet" type="text/css" href="/user-assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/user-assets/fonts/feather/style.min.css">
    <link rel="stylesheet" type="text/css" href="/user-assets/fonts/line-awesome/css/line-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="/user-assets/fonts/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="/user-assets/css/pace.css">
    <link rel="stylesheet" type="text/css" href="/user-assets/css/bootstrap-extended.min.css">
    <link rel="stylesheet" type="text/css" href="/user-assets/css/colors.min.css">
    <link rel="stylesheet" type="text/css" href="/user-assets/css/components.min.css">
    <link rel="stylesheet" type="text/css" href="/user-assets/css/vertical-menu.min.css">
    <link rel="stylesheet" type="text/css" href="/user-assets/css/palette-gradient.min.css">
    <link rel="stylesheet" type="text/css" href="/user-assets/css/palette-gradient.min.css">
    <link rel="stylesheet" type="text/css" href="/user-assets/plugins/toastr/toastr.css">
    <link rel="stylesheet" type="text/css" href="/user-assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="/user-assets/plugins/icheck/icheck.css">
    <link rel="stylesheet" type="text/css" href="/user-assets/plugins/icheck/custom.css">
    <link rel="stylesheet" type="text/css" href="/user-assets/plugins/pick-a-date/default.css">
    <link rel="stylesheet" type="text/css" href="/user-assets/plugins/pick-a-date/default.date.css">
    <link rel="stylesheet" type="text/css" href="/user-assets/plugins/pick-a-date/default.time.css">
    <link rel="stylesheet" href="/user-assets/css/chat.css">
    @yield('import-css')
    @yield('page-css')
</head>

<body class="horizontal-layout horizontal-menu 2-columns  " data-open="hover" data-menu="horizontal-menu" data-color="bg-info" data-col="2-columns">
@include('user.template.header')
@include('user.template.top-menu-bar')
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        @yield('content')
    </div>
</div>
@include('user.template.footer')
<script src="/user-assets/js/vendors.min.js" type="text/javascript"></script>
<script src="/user-assets/js/app-menu.js" type="text/javascript"></script>
<script src="/user-assets/js/app.js" type="text/javascript"></script>
<script src="/user-assets/plugins/toastr/toastr.min.js" type="text/javascript"></script>
<script src="/user-assets/plugins/icheck/icheck.min.js" type="text/javascript"></script>
<script src="/user-assets/js/checkbox-radio.min.js" type="text/javascript"></script>
<script src="/user-assets/plugins/pick-a-date/picker.js" type="text/javascript"></script>
<script src="/user-assets/plugins/pick-a-date/picker.date.js" type="text/javascript"></script>
<script src="/user-assets/plugins/pick-a-date/picker.time.js" type="text/javascript"></script>
<script src="/user-assets/plugins/pick-a-date/legacy.js" type="text/javascript"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>




@yield('import-javascript')
@yield('page-javascript')
<script>
    {{--@if(session('errors'))--}}
    {{--toastr.error('{{ session('errors')->first() }}', "Error");--}}
    {{--@elseif(session('error'))--}}
    {{--toastr.error('{{ session('error') }}', "Oops..!!");--}}
    {{--@elseif(session('success'))--}}
    {{--toastr.success('{{ session('success') }}', "Hurray..!!");--}}
    {{--@endif--}}

    @if(session('errors'))
swal('Oops', '{{ session('errors')->first() }}', 'error');
    @elseif(session('error'))
swal('Oops', '{{ session('error') }}', 'error');
    @elseif(session('success'))
swal('Hurray', '{{ session('success') }}', 'success');
    @endif
</script>
<script src="/user-assets/plugins/axios/axios.min.js"></script>

{{--<script src="/user-assets/plugins/typeahead/typeahead.js"></script>--}}
</body>
</html>