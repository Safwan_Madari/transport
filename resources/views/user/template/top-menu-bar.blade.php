<div class="header-navbar navbar-expand-sm navbar navbar-horizontal navbar-fixed navbar-dark navbar-without-dd-arrow navbar-shadow" role="navigation" data-menu="menu-wrapper">
    <div class="navbar-container main-menu-content" data-menu="menu-container">
        <ul class="nav navbar-nav" id="main-menu-navigation" data-menu="menu-navigation">

            <li class="nav-item">
                <a class="dropdown-toggle nav-link" href="{{ route('user-dashboard') }}">
                    <i class="ft-home"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="dropdown-toggle nav-link" href="">
                    <i class="ft-home"></i>
                    <span>Find Load</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="dropdown-toggle nav-link" href="{{ route('user-post-load-create') }}">
                    <i class="ft-home"></i>
                    <span>Post Load</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="dropdown-toggle nav-link" href="">
                    <i class="ft-home"></i>
                    <span> Find Truck</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="dropdown-toggle nav-link" href="">
                    <i class="ft-home"></i>
                    <span>Calculate Freight</span>
                </a>
            </li>
            <li class="dropdown nav-item" data-menu="dropdown">
                <a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown">
                    <i class="ft-monitor"></i><span>Buy / Sell</span>
                </a>
                <ul class="dropdown-menu">
                    <div class="arrow_box">
                        <li class="dropdown dropdown-submenu" data-menu="dropdown-submenu">
                            <a class="dropdown-item dropdown-toggle" href="#" data-toggle="dropdown">Buy Vehicle</a>
                        </li>
                        <li class="dropdown dropdown-submenu" data-menu="dropdown-submenu">
                            <a class="dropdown-item dropdown-toggle" href="#" data-toggle="dropdown">Sell Vehicle</a>
                        </li>
                    </div>
                </ul>
            </li>
            <li class=" nav-item" data-menu="dropdown">
                <a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown">
                    <i class="ft-monitor"></i>
                    <span>Driver Corner</span>
                </a>
                <ul class="dropdown-menu">
                    <div class="arrow_box">
                        <li class="dropdown dropdown-submenu" data-menu="dropdown-submenu">
                            <a class="dropdown-item dropdown-toggle" href="#" data-toggle="dropdown">Register Driver</a>
                        </li>
                        <li class="dropdown dropdown-submenu" data-menu="dropdown-submenu">
                            <a class="dropdown-item dropdown-toggle" href="#" data-toggle="dropdown"> Find Driver </a>
                        </li>
                        <li class="dropdown dropdown-submenu" data-menu="dropdown-submenu">
                            <a class="dropdown-item dropdown-toggle" href="#" data-toggle="dropdown"> Post Driver Vacancy</a>
                        </li>
                        <li class="dropdown dropdown-submenu" data-menu="dropdown-submenu">
                            <a class="dropdown-item dropdown-toggle" href="#" data-toggle="dropdown">  Find Driver Job</a>
                        </li>
                    </div>
                </ul>
            </li>
            <li class="dropdown nav-item" data-menu="dropdown">
                <a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown">
                    <i class="ft-monitor"></i><span>RTO Corner</span>
                </a>
                <ul class="dropdown-menu">
                    <div class="arrow_box">
                        <li class="dropdown dropdown-submenu" data-menu="dropdown-submenu">
                            <a class="dropdown-item dropdown-toggle" href="#" data-toggle="dropdown">Forms</a>
                        </li>

                    </div>
                </ul>
            </li>
            <li class="dropdown nav-item" data-menu="dropdown">
                <a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown">
                    <i class="ft-monitor"></i><span>More Services</span>
                </a>
                <ul class="dropdown-menu">
                    <div class="arrow_box">
                        <li class="dropdown dropdown-submenu" data-menu="dropdown-submenu">
                            <a class="dropdown-item dropdown-toggle" href="#" data-toggle="dropdown">Transporter Directory</a>
                        </li>
                        <li class="dropdown dropdown-submenu" data-menu="dropdown-submenu">
                            <a class="dropdown-item dropdown-toggle" href="#" data-toggle="dropdown">Insurance Calculator</a>
                        </li>
                        <li class="dropdown dropdown-submenu" data-menu="dropdown-submenu">
                            <a class="dropdown-item dropdown-toggle" href="#" data-toggle="dropdown">How We Works.?</a>
                        </li>

                    </div>
                </ul>
            </li>

        </ul>
    </div>
</div>